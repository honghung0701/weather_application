import React, { useEffect, useState } from "react";
import { useStateContext } from "../Context";
//images
import Clear from "../assets/images/clear.mp4";
import Fog from "../assets/images/fog.mp4";
import Cloudy from "../assets/images/cloudy.mp4";
import Rainy from "../assets/images/rainy.mp4";
import Snow from "../assets/images/snow.mp4";
import Stormy from "../assets/images/stormy.mp4";
import Sunny from "../assets/images/sunny.mp4";

const BackgroundLayout = () => {
  const { weather } = useStateContext();
  const [image, setImage] = useState();

  useEffect(() => {
    if (weather.conditions) {
      let imageString = weather.conditions;
      if (imageString.toLowerCase().includes("clear")) {
        setImage(Clear);
      } else if (imageString.toLowerCase().includes("cloud")) {
        setImage(Cloudy);
      } else if (
        imageString.toLowerCase().includes("rain") ||
        imageString.toLowerCase().includes("shower")
      ) {
        setImage(Rainy);
      } else if (imageString.toLowerCase().includes("snow")) {
        setImage(Snow);
      } else if (imageString.toLowerCase().includes("fog")) {
        setImage(Fog);
      } else if (
        imageString.toLowerCase().includes("thunder") ||
        imageString.toLowerCase().includes("storm")
      ) {
        setImage(Stormy);
      }
    }
  }, [weather]);

  return (
    <video
      autoPlay
      muted
      alt="weather_image"
      className=" w-full fixed left-0 top-0 -z-[10]"
      loop
      src={image}
      type="video/mp4"
    ></video>
  );
};

export default BackgroundLayout;
